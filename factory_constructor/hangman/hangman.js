var $ = function(id) {return document.getElementById(id);};

function Hangman(){
	this.lose = false;
	this.win = false;
	this.guesses = 0;
	this.correctGuesses = 0;
	this.word = '';
	this.currentWord = [];
	this.guessedLetters = [];
	this.gameBoard = [];
	this.wordArray = ['apple', 'orange', 'banana', 'grapes'];
}

Hangman.prototype.setupGame = function(){
	var random = Math.floor(Math.random()*this.wordArray.length);
	var word = this.wordArray[random];
	var currentWord = word.split('');
	var gameBoard = []

	currentWord.forEach(function(){
		gameBoard.push(' _ ');
	});

	this.word = word;
	this.currentWord = currentWord;
	this.gameBoard = gameBoard;
	this.updateGame();

	console.log(this);

}



Hangman.prototype.updateGame = function(){

	var displayGameboard = '';
	var displayGuessedLetters = '';

	//Bind to HTML elements
	var gameBoardHTML = $('gameBoard');
	var letter = $('letter');
	var status = $('status');
	var button = $('guess');
	var totalGuesses = $('total');
	var guessedLetters = $('guessed');


	
		
	//Update HTML elements
	this.gameBoard.forEach(function(space){
		displayGameboard += '<span style="font-size: 36px;">' + space + '</span>';
	});

	this.guessedLetters.forEach(function(letter){
		displayGuessedLetters += '<span style="font-size: 36px;">' + letter + '</span>';
	});

	letter.value = '';
	status.innerHMTL = '<h1>Make your guess</h1>';
	gameBoardHTML.innerHTML = displayGameboard;
	guessedLetters.innerHTML = 'Letters Guessed: ' + displayGuessedLetters;
	
	if(this.guesses < 8){
		status.innerHTML = 'Guess again!';
		totalGuesses.innerHTML = 'Total Guesses: ' + hangman.guesses;
	}
	

	
	//Examing Win/Lose Condition
	if(this.correctGuesses === this.currentWord.length){
		this.win = true;
		this.lose = false;
		if(this.win){
			console.log("You win!");
			status.innerHTML = 'You Win!';
			button.disabled = true;
		}
				
	}else if(this.guesses > 8 ){
		this.win = false;
		this.lose = true;
		if(this.lose){
			console.log("You lose!");
			status.innerHTML = 'You Lose!';
			button.disabled = true;
		}
	}

}


Hangman.prototype.makeGuess = function(){

	var letter = $('letter').value;
	var response = this.currentWord.indexOf(letter);
	
	//Examine Guess
	if(response === -1){
		this.guesses  += 1;
	}
	else{
		this.guesses  += 1;
		this.correctGuesses += 1;
		this.gameBoard[response] = letter;
		this.currentWord[response] = '*';
	}
	this.guessedLetters.push(letter);
	this.updateGame();
}


var hangman = new Hangman();
hangman.setupGame();



