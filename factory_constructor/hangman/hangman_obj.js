var $ = function(id) {return document.getElementById(id);};

//let $ = (selector)=> return document.querySelector(selector);

var Hangman = {
	lose:false,
	win: false,
	guesses:0,
	correctGuesses: 0,
	word:'',
	currentWord:[],
	gameBoard :[],
	wordArray : ['apple', 'orange', 'banana', 'grapes'],
	setupGame: setupGame,
	updateGame: updateGame,
	makeGuess: makeGuess
}

function setupGame (){
	var game = {
		word:'',
		currentWord: [],
		gameBoard: []
	}

	var random = Math.floor(Math.random()*this.wordArray.length);

	
	game.word = this.wordArray[random];
	game.currentWord = game.word.split('');
	game.gameBoard = []

	game.currentWord.forEach(function(){
		game.gameBoard.push(' _ ');
	});

	this.word = game.word;
	this.currentWord = game.currentWord;
	this.gameBoard = game.gameBoard;

	return game;

}


function updateGame (){
	//console.log(this.gameBoard);
	var gameBoardHTML = $('gameBoard');
	
	//Examing Win/Lose Condition
	if(Hangman.correctGuesses === Hangman.currentWord.length){
		Hangman.win = true;
		Hangman.lose = false;
	}else if(Hangman.guesses > 8 || Hangman.guesses === Hangman.currentWord.length){
		Hangman.win = false;
		Hangman.lose = true;
	}
	//gameBoardHTML.innerHTML = hangman.gameBoard;
}


function makeGuess (guess){

	var response = Hangman.currentWord.indexOf(guess);

	//Examine Guess
	if(response === -1){
		Hangman.guesses  += 1;
	}
	else{
		Hangman.guesses  += 1;
		Hangman.correctGuesses += 1;
		Hangman.gameBoard[response] = guess;
		Hangman.currentWord[response] = '*';
	}

	Hangman.updateGame();
}


var hangman = Object.create(Hangman);
hangman.setupGame();

for(x in hangman){
	if(typeof(hangman[x]) !== 'function'){
		console.log( x + ':' + hangman[x]);
	}
}

//console.log(hangman);
