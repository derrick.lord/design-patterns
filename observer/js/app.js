const model = new NumberModel();
const eObs1 = new ElementObserver('elementObserver1');
const cObs = new ConsoleObserver();
const hObs = new HistoryObserver();

model.addObserver(eObs1);
model.addObserver(cObs);
model.addObserver(hObs);
model.notifyObservers();