class NumberModel {
	constructor(){
		this.number = 0;
		this.color = 'red';
		this.observer = [];
		this.observerPos = [];
		this.reset = false;
		this.decrease = false;
	}

	increment(){
		const colors = ['orange', 'red', 'green', 'blue'];
		this.number++;
		this.color = colors[Math.floor(Math.random() * colors.length)];

		this.notifyObservers();
	}

	decrement(){
		if(this.number > 0){
			this.number--;
			this.decrease = true;
			this.notifyObservers();
		}else{
			this.number = 0;
			this.color = 'red';
			alert('number cannot be less than 0');
		}
	}

	clear(){
		this.number = 0;
		this.color = 'red';
		this.reset = true;

		this.notifyObservers();
	}

	addObserver(o){
		//Add object to object observer array
		this.observer.push(o);
		//Add object to object position array if a HTML element w/ ID
		if(o.element){
			this.observerPos.push({
				id: o.element.id,
				pos: o.element.getBoundingClientRect()
			});
		}
	}

	notifyObservers(){
		for(let o of this.observer){
			o.update(this);
		}
	}
}