class HistoryObserver{
	constructor(){
		this.colorHistory = [];
	}

	display(){
		let msg = 'The most recent 5 colors were: ';
		for(let i = 0; i < 5; i++){
			if(this.colorHistory[i]){
				msg += this.colorHistory[i]+ ' ';
			}
		}
		console.log(msg);
	}

	update(model){
		if(model.reset){
			this.colorHistory = [];
			model.color = 'red';
			model.reset = false;
			return console.clear();
			//return console.log('The model has been reset');
		}

		if(model.decrease){
			model.decrease = false;
			this.colorHistory.shift();
			(this.colorHistory > 0 )?model.color = this.colorHistory[0].toLowerCase():model.color = 'red';
			return this.display();
		}

		this.colorHistory.unshift(model.color.toUpperCase());
		this.display();
	}
}